package it.atenainformatica.icare.soggetto;

import groovy.sql.Sql;
import com.atena.dynform.server.ws.WSHTTPPost;
import com.webratio.rtx.core.BeanHelper;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.soap.MimeHeaders
import org.dom4j.Node;
import java.text.DateFormat
import java.text.SimpleDateFormat
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.text.ParseException;
import java.nio.charset.StandardCharsets

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

def configuration = rtx.getConfiguration();
italy = configuration.getStringProperty("IT","100");

/*prod*/
//COM_ISTAT = "014061";
/*svil*/
COM_ISTAT = "092009";
COM_DESCR = "CAGLIARI";
COM_PROV = "CA";


UPDATE_OPERATION = StringUtils.startsWith(importOperation,"UPDATE_");

//Ricera soggetto
getSoggetto =   "GET_SOGGETTO".equals(importOperation);
updateSoggetto = "UPDATE_SOGGETTO".equals(importOperation);
updateFamiglia = "UPDATE_FAMIGLIA".equals(importOperation);
getFamigliaOp = "GET_FAMIGLIA".equals(importOperation)

/** Composizione del documento da ritornare */
Document resultDoc= DocumentHelper.createDocument();
Element elencoEl = resultDoc.addElement("ElencoSoggetti");
def result = ["resultCode":"error"];


if (getSoggetto && StringUtils.isEmpty(soggettoCognome) && StringUtils.isEmpty(soggettoNome) && StringUtils.isEmpty(soggettoCF)) {
    result.put("message", "<p>Compilare almeno uno dei campi di filtraggio</br> cognome, nome o codice fiscale.</p>");
    return result;
}

if (getSoggetto && !StringUtils.isEmpty(soggettoNome) && StringUtils.isEmpty(soggettoCognome) && StringUtils.isEmpty(soggettoCF)) {
    result.put("message", "<p>Ricerca solo per NOME non consentita.</p>");
    return result;
}

if (getSoggetto && !StringUtils.isEmpty(soggettoCF) && soggettoCF.length() < 16) {
    result.put("message", "<p>Codice fiscale troppo breve.</p>");
    return result;
}

if (getSoggetto && !StringUtils.isEmpty(soggettoCognome) && soggettoCognome.length() <= 2) {
    result.put("message", "<p>Cognome troppo breve, numero minimo di caratteri 3.</p>");
    return result;
}

if (getSoggetto && !StringUtils.isEmpty(soggettoNome) && soggettoNome.length() <= 2) {
    result.put("message", "<p>Nome troppo breve, numero minimo di caratteri 3.</p>");
    return result;
}


if (getSoggetto && !StringUtils.isEmpty(soggettoCF) && soggettoCF.length() < 16) {
    result.put("message", "<p>Codice fiscale troppo breve, numero minimo di caratteri ammessi 16.</p>");
    return result;
}



if (getSoggetto || updateSoggetto || updateFamiglia || getFamigliaOp) {

 
    //List cfNucleo = getNucleoSoggetto("VNRDNL78H02I828E");



    /*println("N individuale")
    println(nIndividuale);
    println("getSoggetto: " +getSoggetto)
    println("updateSoggetto: " +updateSoggetto)
    println("getFamigliaOp: " +getFamigliaOp)
    println("updateFamiglia: " +updateFamiglia)
    println(soggettoCF +  " " + nIndividuale)*/

    if(updateSoggetto || getSoggetto){
        List soggetti = getSingoloSoggetto();
        for (int x = 0; x < soggetti.size(); x++) {
            elencoEl.add(getXMLSoggettoFromJSON(soggetti.get(x)));
        }
    }

    if(getFamigliaOp || updateFamiglia){
        String cf = null;
        if(soggettoNucleo == null){
            cf = getCfFromMatricola(nIndividuale);
        }
        List cfNucleo = getNucleoSoggetto(cf)
        for (int c; c < cfNucleo.size(); c++) {
            List soggetti = getSingoloSoggetto(cfNucleo.get(c));
            for (int x = 0; x < soggetti.size(); x++) {
                elencoEl.add(getXMLSoggettoFromJSON(soggetti.get(x)));
            }
        }
    }



}


result = ["resultCode": "success", "document": resultDoc];

return result;








def getSingoloSoggetto(String cfSingoloSogg) {
    accessToken = refreshAccessToken();

    URL url = new URL(urlRicercaIndividui);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();

    initRequestConnection(con, accessToken);


    Map<String, String> arguments = [:];

    if (cfSingoloSogg != null) {
        arguments.put("codiceFiscale", cfSingoloSogg);
    } else {

        if (!StringUtils.isEmpty(soggettoCF)) {
            arguments.put("codiceFiscale", soggettoCF);
        } else {
            String cognomeNome = soggettoCognome + "% " + soggettoNome + "%";
            arguments.put("CognomeNome", cognomeNome);
        }
    }

    //arguments.put("Cognome", "andrea");
    arguments.put("mostraIndirizzo", "True");
    arguments.put("MostraDatiCartaIdentita", "True");
    arguments.put("MostraDatiIscrizione", "True");
    arguments.put("MostraDatiStatoCivile", "True");
    arguments.put("EscludiStatoAnagraficoOccasionale", "True");
    arguments.put("MostraDatiTitoloSoggiorno","True");
arguments.put("MostraDatiDecesso", "True");
    setRequestParams(con, arguments);


    int responseCode = con.getResponseCode();

    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))
    StringBuilder response = new StringBuilder();
    String responseLine = null;
    while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
    }



    JSONParser jsonParser = new JSONParser();
    Object obj;
    List soggetti = new ArrayList();
    try {
        obj = jsonParser.parse(response.toString());
        JSONArray soggettiList = (JSONArray) obj;
        for (Object o : soggettiList) {
            if (o instanceof JSONObject) {
                JSONObject oJ = (JSONObject) o;
                soggetti.add(oJ);
            }
        }


    } catch (ParseException e) {
        e.printStackTrace();
    }

    return soggetti;
}



String notNull(Object value) {
    if(value == null)
        return "";
    if(StringUtils.isEmpty(""+value) || "null".equals(StringUtils.trim(""+value)))
        return "";
    return ""+value;
}

String getValue(Element parent,String name){
    Element child = parent.selectSingleNode("*[local-name()='"+name+"']")
    if(child == null) return null;
    return child.getTextTrim();
}


String getValueDate(Element parent,String name){
    String dateStr = getValue(parent, name);
    if(StringUtils.isEmpty(dateStr) || (dateStr.length()  < 8))
        return null;
    if("00000000".equals(dateStr))
        return "0000-01-01"; //Data incompleta
    String year = dateStr.substring(0, 4);
    String month = dateStr.substring(4, 6);
    String day = dateStr.substring(6, 8);

    if("00".equals(month) || "00".equals(day)){
        return "0001-01-01"; //Data incompleta
    }
    return year+"-"+month+"-"+day;
}

Element createElement(Element rootEl,String name, Object value){
    Element el = DocumentHelper.createElement(name);
    if(!StringUtils.isEmpty(value)|| "null".equals(StringUtils.trim(value))){
        el.setText(notNull(value));
    }
    rootEl.add(el);
    return el;
}

private static String toXPath(QName qName) {
    return "*[local-name()='" + qName.getName() + "' and namespace-uri()='" + qName.getNamespaceURI() + "']";
}

private String xpath(String xml, String xpathExpression) {
    try {
        //string to w3c document
        def builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        ByteArrayInputStream inputStream = new ByteArrayInputStream(xml.getBytes("UTF-8"))
        def doc = builder.parse(inputStream).getDocumentElement();
        //evaluate
        def xpath = XPathFactory.newInstance().newXPath()
        return xpath.evaluate(xpathExpression, doc)
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null
}

private String xpath(Element doc, String xpathExpression) {
    try {
        XPath xpath = XPathFactory.newInstance().newXPath()
        return xpath.evaluate(xpathExpression, doc)
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null
}

String comuneISTAT(String cod){
    cod = StringUtils.defaultString(cod,"");
    if("0".equals(cod)) return null;
    String codice = StringUtils.stripStart(cod,"0");

    return cod;
}

String comuneDescr(String cod){
    cod = StringUtils.defaultString(cod,"");
    if(COM_ISTAT.equals(cod))
        return COM_DESCR;
    return cod;
}

String getNazioneOidByISTAT(String codiceIstat){
    if(StringUtils.isEmpty(codiceIstat)) return "";
    if(italy.equals(codiceIstat)) return italy;

    def dbSession = getDBSession("db1");
    def query = dbSession.createSQLQuery("SELECT oid_2 FROM nazionetable  WHERE codice_istat = :codiceIstat");
    query.setString("codiceIstat",codiceIstat).setMaxResults(1);
    String nazioneOid = BeanHelper.asString(query.uniqueResult());
    commit(dbSession);
    return nazioneOid;
}


int _codiceStatoCivile(String cod) {
    if(StringUtils.isEmpty(cod)) return 0;

    if("*".equals(cod))  return 1;
    else if("C".equals(cod))  return 2;
    else if("D".equals(cod))  return 3;
    else if("L".equals(cod))  return 4;
    else if("N".equals(cod))  return 5;
    else if("P".equals(cod))  return 6;
    else if("V".equals(cod))  return 7;
    return 0;
}


ArrayList<String> removeDuplicates(ArrayList<String> list) {

    ArrayList<String> result = new ArrayList<String>();


    HashSet<String> set = new HashSet<String>();


    for (String item : list) {

        if (!set.contains(item)) {
            result.add(item);
            set.add(item);
        }
    }
    return result;
}



String formatData(String dataS){
    if(StringUtils.isEmpty(dataS) || (dataS.length()  < 8)){
        dataS = null;
    }else{
        String year = dataS.substring(0, 4);
        String month = dataS.substring(4, 6);
        String day = dataS.substring(6, 8);
        //Gestione data nascita incompleta.
        if("0000".equals(year) || "00".equals(month) || "00".equals(day)){
            dataS = "0001-01-01";
        }else{
            dataS = year+"-"+month+"-"+day;
        }
    }

    return dataS;
}


def getNucleoSoggetto(String cf) {

    accessToken = refreshAccessToken();


    URL url = new URL(urlRicercaComponentiFamiglia);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();

    initRequestConnection(con, accessToken);


    Map<String, String> arguments = [:]


    if(soggettoNucleo != null){
        arguments.put("CodiceAggregazione", soggettoNucleo);
    }else{
        if (cf != null) {
            arguments.put("CodiceFiscaleComponente", cf);
        } else {
            arguments.put("CodiceFiscaleComponente", soggettoCF);
        }
    }




    setRequestParams(con, arguments);


    int responseCode = con.getResponseCode();

    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))
    StringBuilder response = new StringBuilder();
    String responseLine = null;
    while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
    }

    JSONParser jsonParser = new JSONParser();
    Object obj;
    List soggetti = new ArrayList();
    try {
        obj = jsonParser.parse(response.toString());
        JSONArray soggettiList = (JSONArray) obj;
        for (Object o : soggettiList) {
            if (o instanceof JSONObject) {
                JSONObject oJ = (JSONObject) o;
                soggetti.add((String) oJ.get("codiceFiscale"));
            }
        }


    } catch (ParseException e) {
        e.printStackTrace();
    }

    return soggetti;
}



def refreshAccessToken() {
    String token = null;

    try {


        URL url = new URL(urlAccess);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);


        Map<String, String> arguments = [:]
        arguments.put("grant_type", "client_credentials");
        arguments.put("client_id", clinetId);
        arguments.put("client_secret", clientSecret);
        arguments.put("resource", resource);




        String separator = "&";
        boolean first = true;
        String argumentsString = "";
        for (Map.Entry<String, String> entry : arguments.entrySet()) {
            if (!first)
                argumentsString = argumentsString + separator;
            argumentsString = argumentsString + URLEncoder.encode(entry.getKey(), "UTF-8") + "=" + URLEncoder.encode(entry.getValue(), "UTF-8");
            first = false;
        }
        byte[] out = argumentsString.toString().getBytes(StandardCharsets.UTF_8);







        int length = out.length;

        con.setFixedLengthStreamingMode(length);
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        con.connect();
        OutputStream os = con.getOutputStream();
        os.write(out);



        int responseCode = con.getResponseCode();


        if (responseCode == 400)
            return null;

        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))
        StringBuilder response = new StringBuilder();
        String responseLine = null;
        while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
        }

        try {
            JSONParser jsonParser = new JSONParser();
            Object obj = jsonParser.parse(response.toString());
            JSONObject accessResponse = (JSONObject) obj;
            token = (String) accessResponse.get("access_token");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    } catch (MalformedURLException e) {
        e.printStackTrace();
        return null;
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        return null;
    }


    return token;
}


def initRequestConnection(HttpURLConnection con, String accessToken) throws ProtocolException {
    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/json; utf-8");
    con.setRequestProperty("Accept", "application/json");
    con.setRequestProperty("Authorization", "Bearer " + accessToken);
    con.setDoOutput(true);

}

def setRequestParams(HttpURLConnection con, Map<String, String> arguments) throws IOException {


    String separator = "&";
    boolean first = true;
    String argumentsString = "";
    for (Map.Entry<String, String> entry : arguments.entrySet()) {
        if (!first)
            argumentsString = argumentsString + separator;
        argumentsString = argumentsString + URLEncoder.encode(entry.getKey(), "UTF-8") + "=" + URLEncoder.encode(entry.getValue(), "UTF-8");
        first = false;
    }
    byte[] out = argumentsString.toString().getBytes(StandardCharsets.UTF_8);


    int length = out.length;

    con.setFixedLengthStreamingMode(length);
    con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    con.connect();
    OutputStream os = con.getOutputStream()
    os.write(out);

}


def getXMLSoggettoFromJSON(JSONObject oJ) {

println(oJ.toString());


    Element soggetto = null;

    String posizioneAnagrafica = (String) oJ.get("posizioneAnagrafica");
    String fonteUserStatusCode = "";
    if (posizioneAnagrafica.equals("R")) fonteUserStatusCode = "A";
    if (posizioneAnagrafica.equals("D")) fonteUserStatusCode = "D";
    if (posizioneAnagrafica.equals("E")) fonteUserStatusCode = "E";
    if (posizioneAnagrafica.equals("I")) fonteUserStatusCode = "I";
    if (posizioneAnagrafica.equals("A")) fonteUserStatusCode = "R";




    soggetto = DocumentHelper.createElement("AnagraficaSoggetto");


    def codiceStatoIcare;
    codiceStatoIcare = setSoggettoStatusData(soggetto,fonteUserStatusCode);



    createElement(soggetto, "Cognome", (String) oJ.get("cognome"));
    createElement(soggetto, "Nome", (String) oJ.get("nome"));
    createElement(soggetto, "NIndividuale", (String) oJ.get("codiceCittadino"));

    String dataNascita = (String) oJ.get("dataNascita");
    if (StringUtils.isEmpty(dataNascita) || (dataNascita.length() < 8)) {
        dataNascita = null;
        createElement(soggetto, "DataNascitaIncompleta", "1");
    } else {
        String year = dataNascita.substring(6, 10);
        String month = dataNascita.substring(3, 5);
        String day = dataNascita.substring(0, 2);
        //Gestione data nascita incompleta.
        if ("0000".equals(year) || "00".equals(month) || "00".equals(day)) {
            createElement(soggetto, "DataNascitaIncompleta", "1");
            createElement(soggetto, "DataNascitaDichiarata", day + "/" + month + "/" + year);
            dataNascita = "0001-01-01";
        } else {
            createElement(soggetto, "DataNascitaIncompleta", "0");
            dataNascita = year + "-" + month + "-" + day;
        }
    }

    createElement(soggetto, "DataNascita", dataNascita);
    createElement(soggetto, "Sesso", (String) oJ.get("sesso"));
    createElement(soggetto, "Codicefiscale", (String) oJ.get("codiceFiscale"));
    createElement(soggetto, "FonteCertificazioneCFOid", "1");


    createElement(soggetto, "NazioneCittadinanzaPrima", (String) oJ.get("codiceIstatNazioneCittadinanza"));
    createElement(soggetto, "NazioneCittadinanzaPrimaDescr", (String) oJ.get("descrizioneCittadinanza"));

    //Inizio residenza
   JSONObject dI = (JSONObject) oJ.get("datiIscrizione");
	


    if(dI != null){
        String dataDecorrenza = (String) dI.get("dataDecorrenza");
        String year = dataDecorrenza.substring(0, 4);
        String month = dataDecorrenza.substring(5, 7);
        String day = dataDecorrenza.substring(8, 10);
        dataDecorrenza = year + "-" + month + "-" + day;
        createElement(soggetto,"DataInizioResidenza", dataDecorrenza);  
    }


    //Decesso


    JSONObject datiDecesso = (JSONObject) oJ.get("datiDecesso")
    if(datiDecesso != null){

        String dataMorte = (String) datiDecesso.get("data");
        String comuneMorte = (String) datiDecesso.get("comune");
        String year = dataMorte.substring(0, 4);
        String month = dataMorte.substring(5, 7);
        String day = dataMorte.substring(8, 10);
        dataMorte = year + "-" + month + "-" + day;
        createElement(soggetto, "DataDecesso", dataMorte);
	
        String istatMorte = getComuneByDescr(comuneMorte);
        createElement(soggetto,"ComuneDecessoDescr", comuneMorte);
        createElement(soggetto,"ComuneDecessoISTAT",istatMorte);
        //createElement(soggetto,"ProvinciaDecesso", getValue(sbjSrc,"ProvinciaDecesso"));

    }

    //Nascita

    if( oJ.get("codiceIstatComuneNascitaItaliano") != null &&  !((String)oJ.get("codiceIstatComuneNascitaItaliano")).isEmpty()) {
        createElement(soggetto, "NazioneNascita", "100");
        createElement(soggetto, "NazioneNascitaDescr", "ITALIA");
        String comuneNascitaDescr =  (String) oJ.get("descrizioneComuneNascitaItaliano")
        createElement(soggetto, "ComuneNascitaDescr",comuneNascitaDescr);

        String comuneNascitaIstat = (String) oJ.get("codiceIstatComuneNascitaItaliano");
println("comuneNascitaIstat: " + comuneNascitaIstat);
        //comuneNascitaIstat = comuneNascitaIstat.substring(1);

        createElement(soggetto, "ComuneNascitaISTAT", comuneNascitaIstat);    

    }else{
        String codiceIstatNazioneNascitaEstero = (String) oJ.get("codiceIstatNazioneNascitaEstero")
        String nazioneEsteraOid = getNazioneOidByCodice(codiceIstatNazioneNascitaEstero);
        String nazioneEsteraDescr = getNazioneDescrByCodice(codiceIstatNazioneNascitaEstero);
        createElement(soggetto, "NazioneNascita",nazioneEsteraOid);
        createElement(soggetto, "NazioneNascitaDescr", nazioneEsteraDescr);
        createElement(soggetto,"ComuneNascitaEsteroDescr",  (String) oJ.get("descrizioneComuneNascitaEstero"));
    }
    

    //Residenza

    createElement(soggetto, "ComuneResidenzaISTAT", COM_ISTAT);
    createElement(soggetto, "ComuneResidenzaDescr", COM_DESCR);
    createElement(soggetto, "ProvinciaResidenza", COM_PROV);
    createElement(soggetto, "NazioneResidenza", "100");
    createElement(soggetto, "NazioneResidenzaDescr", "ITALIA");


    JSONObject datiResidenza = (JSONObject) oJ.get("datiDettaglioIndirizzoNelComune");

    if (datiResidenza != null) {
        createElement(soggetto, "Via", (String) datiResidenza.get("specieVia") + " " + (String) datiResidenza.get("denominazioneVia"));
	        String capResidenza = (String) oJ.get("cap");
        if(capResidenza != null){
            createElement(soggetto, "CapResidenza", capResidenza.substring(1));
        }
        createElement(soggetto, "CodVia", (String) datiResidenza.get("codiceCivico"));
        createElement(soggetto, "Civico", (String) datiResidenza.get("numero"));
        createElement(soggetto, "Scala", (String) datiResidenza.get("scala"));
        createElement(soggetto, "Piano", (String) datiResidenza.get("piano"));
        createElement(soggetto, "Esponente", (String) datiResidenza.get("esponente1"));
        createElement(soggetto, "Interno", (String) datiResidenza.get("interno1"));
        createElement(soggetto, "UnitaUrbana", (String) datiResidenza.get("frazioneDettaglioIndirizzo"));
    }


    //Stato civile

    JSONObject datiStatoCivile = (JSONObject) oJ.get("datiStatoCivile");
    createElement(soggetto, "StatoCivileOid", getStatoCivileFromCodice((String) oJ.get("statoCivile"))) ;

    //Dati famiglia

    createElement(soggetto, "TipoFamiglia", "DE");
    //Codice per Consultazione Nucleo
    createElement(soggetto, "CodiceNucleoFamiglia", (String) oJ.get("codiceFamiglia"));
    createElement(soggetto, "NFamiglia", (String) oJ.get("codiceFamiglia"));
    createElement(soggetto, "NFamigliaOriginale", (String) oJ.get("codiceFamiglia"));
    createElement(soggetto,"Parentela",  getRelazioneParentelaDescr( (String) oJ.get("codiceRelazioneLegame")));

    JSONObject datiIscrizione = (JSONObject) oJ.get("datiIscrizione");

    if (datiIscrizione != null) {

        String dataInizioResidenzaJson = (String) datiIscrizione.get("dataDecorrenza");
        createElement(soggetto,"DataInizioResidenza", detDataFromTs(dataInizioResidenzaJson));
    }



        //Carta identità
        Element cIDIcare = DocumentHelper.createElement("CartaIdentita");

        JSONObject datiCartaIdentita = (JSONObject) oJ.get("datiCartaIdentita")
        if(datiCartaIdentita != null){

            createElement(cIDIcare,"Matricola",(String) oJ.get("codiceCittadino"));
            createElement(cIDIcare,"DataRilascio",detDataFromTs((String) datiCartaIdentita.get("dataRilascio")));
            createElement(cIDIcare,"DataScadenza",detDataFromTs((String) datiCartaIdentita.get("dataScadenza")));
            createElement(cIDIcare,"Numero",(String) datiCartaIdentita.get("numero"));
            if(oJ.get("motivoRilascio") != null)
                createElement(cIDIcare,"MotivoRilascio",(String) datiCartaIdentita.get("motivoRilascio"));


            String comuneRilascioS = (String) datiCartaIdentita.get("comuneRilascio");
            createElement(cIDIcare,"ComuneISTAT",null);
            createElement(cIDIcare,"ComuneDescr",comuneRilascioS);
            createElement(cIDIcare,"Provincia",null);
        }

        soggetto.add(cIDIcare);




        //Permesso soggiorno
        Element permessoSoggiorno = DocumentHelper.createElement("PermessoSoggiorno");

        JSONObject permessoSoggiornooJ = (JSONObject) oJ.get("datiTitoloSoggiorno")
        if(permessoSoggiornooJ != null){
            createElement(permessoSoggiorno,"Tipologia",(String) permessoSoggiornooJ.get("tipo"));
            createElement(permessoSoggiorno,"Matricola",(String) oJ.get("codiceCittadino"));
            createElement(permessoSoggiorno,"DataRilascio",detDataFromTs((String) permessoSoggiornooJ.get("dataRilascio")));
            createElement(permessoSoggiorno,"DataScadenza",detDataFromTs((String) permessoSoggiornooJ.get("dataScadenza")));
            createElement(permessoSoggiorno,"Numero",(String) permessoSoggiornooJ.get("numero"));



            String comuneRilascioS = (String) permessoSoggiornooJ.get("comuneRilascio");
            createElement(permessoSoggiorno,"ComuneISTAT",null);
            createElement(permessoSoggiorno,"ComuneDescr",comuneRilascioS);
            createElement(permessoSoggiorno,"Provincia",null);
        }

        soggetto.add(permessoSoggiorno);




    return soggetto;
}

String detDataFromTs(String s){
    s = s.substring(0,12);

    String year = s.substring(0, 4);
    String month = s.substring(5, 7);
    String day = s.substring(8, 10);
    s = year + "-" + month + "-"+ day;
    return  s;
}


String getComuneByDescr(String descr) {
    if(StringUtils.isEmpty(descr)) return "";
    def dbSession = getDBSession("db1");
    def query = dbSession.createSQLQuery("SELECT codice_istat FROM comunetable where nome = :descr");
    query.setString("descr",descr).setMaxResults(1);
    String comuneOid = BeanHelper.asString(query.uniqueResult());
    commit(dbSession);
    return comuneOid;
}

String getNazioneDescrByCodice(String codice) {
    if(StringUtils.isEmpty(codice)) return "";

    def dbSession = getDBSession("db1");
    def query = dbSession.createSQLQuery("SELECT nazione FROM nazionetable where codice_istat =  :codice");
    query.setString("codice",codice).setMaxResults(1);
    String res = BeanHelper.asString(query.uniqueResult());
    commit(dbSession);
    return res;
}

String getNazioneOidByCodice(String codice) {
    if(StringUtils.isEmpty(codice)) return "";

    def dbSession = getDBSession("db1");
    def query = dbSession.createSQLQuery("SELECT oid_2 FROM nazionetable where codice_istat =  :codice");
    query.setString("codice",codice).setMaxResults(1);
    String res = BeanHelper.asString(query.uniqueResult());
    commit(dbSession);
    return res;
}

/*

String detDataFromTs(String s){
    s = s.substring(0,12);

    String year = s.substring(0, 4);
    String month = s.substring(5, 7);
    String day = s.substring(8, 10);
    s = day + "/" + month + "/"+ year;
    return  s;
}
*/

def getCfFromMatricola(String matricola) {
    accessToken = refreshAccessToken();

    URL url = new URL(urlRicercaIndividui);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();

    initRequestConnection(con, accessToken);


    Map<String, String> arguments =  [:]
    arguments.put("CodiceCittadino", matricola);
    arguments.put("EscludiStatoAnagraficoOccasionale", "True");

    setRequestParams(con, arguments);


    int responseCode = con.getResponseCode();

    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))
    StringBuilder response = new StringBuilder();
    String responseLine = null;
    while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
    }
    
    JSONParser jsonParser = new JSONParser();
    Object obj;
    List soggetti = new ArrayList();
    try {
        obj = jsonParser.parse(response.toString());
        JSONArray soggettiList = (JSONArray) obj;
        for (Object o : soggettiList) {
            if(soggettiList.size() == 1){
                JSONObject oJ = (JSONObject) o;
                return  (String) oJ.get("codiceFiscale")
            }else{
                return  null;
            }

        }


    } catch (ParseException e) {
        e.printStackTrace();
    }

    return null;
}


String getStatoCivileFromCodice(String codice){
    Map<Integer, String> stati;
    stati =  [:]

    stati.put(1,"6");
    stati.put(2,"4");
    stati.put(3,"8");
    stati.put(4,"5");
    stati.put(9,"1");
    stati.put(6,"1");
    stati.put(7,"1");
    stati.put(8,"7");

    String res = null;
    
    Integer codiceInt = Integer.valueOf(codice);
    if(codiceInt != null)
        res = stati.get(codiceInt);
    
    return res;

}


String getRelazioneParentelaDescr(String codice){
    Map<Integer, String> relazioniParentela;
    relazioniParentela =  [:]
    relazioniParentela.put(26,"Tutore");
    relazioniParentela.put(1,"Intestatario Scheda");
    relazioniParentela.put(2,"Marito / Moglie");
    relazioniParentela.put(3,"Figlio / Figlia");
    relazioniParentela.put(4,"Nipote (discendente)");
    relazioniParentela.put(5,"Pronipote (discendente)");
    relazioniParentela.put(6,"Padre / Madre");
    relazioniParentela.put(7,"Nonno / Nonna");
    relazioniParentela.put(8,"Bisnonno / Bisnonna");
    relazioniParentela.put(9,"Fratello / Sorella");
    relazioniParentela.put(10,"Nipote (collaterale)");
    relazioniParentela.put(11,"Zio / Zia (Collaterale)");
    relazioniParentela.put(12,"Cugino / Cugina");
    relazioniParentela.put(13,"Altro Parente");
    relazioniParentela.put(16,"Genero / Nuora");
    relazioniParentela.put(17,"Suocero / Suocera");
    relazioniParentela.put(18,"Cognato / Cognata");
    relazioniParentela.put(20,"Nipote (Affine)");
    relazioniParentela.put(21,"Zio / Zia (Affine)");
    relazioniParentela.put(22,"Altro Affine");
    relazioniParentela.put(23,"Convivente (con vincoli di adozione o affettivi)");
    relazioniParentela.put(24,"Responsabile della convivenza non affettiva");
    relazioniParentela.put(25,"Convivente in convivenza non affettiva");
    relazioniParentela.put(99,"Non definito/comunicato");
    relazioniParentela.put(14,"Figliastro / Figliastra");
    relazioniParentela.put(15,"Patrigno / Matrigna");
    relazioniParentela.put(19,"Fratellastro / Sorellastra");
    relazioniParentela.put(80,"Adottato");
    relazioniParentela.put(81,"Nipote");
    relazioniParentela.put(28,"Unito civilmente");
    String relParentela = null;
    
    Integer cod = Integer.valueOf(codice);
    if(cod != null)
        relParentela = relazioniParentela.get(cod);
    
    return relParentela;
}
